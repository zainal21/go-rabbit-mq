package main

import (
	"context"
	message_queue "go-rabbit-mq/message-queue"
	"log"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

func main() {
	//  connection to RabbitMQ
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	// Create a context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Initialize the consumer
	consumer := message_queue.NewConsumer(ctx, conn)

	// Define the message handler
	onMessage := func(msg amqp.Delivery) error {
		log.Printf("Received a message: %s", msg.Body)
		return nil
	}

	// Start consuming messages
	err = consumer.Consume("test_queue", onMessage)
	if err != nil {
		log.Fatalf("Failed to consume messages: %v", err)
	}

	// Keep the main function alive to keep consuming messages
	select {}
}
