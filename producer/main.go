package main

import (
	"context"
	message_queue "go-rabbit-mq/message-queue"
	"log"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

func main() {
	// Establish connection to RabbitMQ
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	// Create a context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Initialize the producer
	producer := message_queue.NewProducer(ctx, conn)

	// Define the payload
	payload := map[string]string{
		"message": "Hello, RabbitMQ!",
	}

	// Publish the message
	err = producer.Publish("test_queue", payload)
	if err != nil {
		log.Fatalf("Failed to publish message: %v", err)
	}

	log.Println("Message published successfully!")
}
